class TestTypecheck {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {

// Check typecheck, should reject floats.
    public int f(){
	int result;
	int count;
	boolean done;
	result = 0;
	count = 1;
	done = false;
	while (count != true) {
	    result = result + count;
	    done = !(count != 10);
	    count = count + 1;
	}
	return result;
    }
}