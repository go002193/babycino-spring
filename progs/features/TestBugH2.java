class TestBugH2 {
    public static void main(String[] a) {
        System.out.println(new Test().f());
    }
}

class Test {

    public int f() {
        int result;
        int count;
        boolean done;
        result = 0;
        count = 11;
        done = false;
        do {
            if (10 < count) {
                done = true;
                System.out.println(done);
            }

            else {
            result = result + count;
            count = count + 1;
            }
        } while (!done);
        return result;
    }

}
